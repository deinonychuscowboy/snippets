# This is a script to export an outlook calendar to ical format so it can be used (in read only mode) by an email client that doesn't support EWS
# Syncing exchange calendars with IMAP clients shouldn't be as hard as it is
# In addition, a lot of google/stackoverflow results for this problem don't use the best/most recent means of doing this and try to create the ical file manually instead or do some other weird hackish thing
# Outlook has a built-in ical exporter now that will correctly handle things like meeting participants for you, let it be M$'s problem, not yours
$output_file = "C:\Calendar.ics"

# Antivirus application needs to start up before Outlook will allow this script to run automatically, so pause in case we just logged in and programs are still starting
# (You may still get an error depending on your group policy settings)
Start-Sleep -s 30
# Load outlook COM object
Add-type -assembly "Microsoft.Office.Interop.Outlook" | out-null
# Get calendar items
$olFolders = "Microsoft.Office.Interop.Outlook.OlDefaultFolders" -as [type]
$outlook = new-object -comobject outlook.application
$namespace = $outlook.GetNameSpace("MAPI")
$folder = $namespace.getDefaultFolder($olFolders::olFolderCalendar)
# Create and configure the exporter object
$exporter = $folder.getCalendarExporter()
$exporter.CalendarDetail = 2
$exporter.IncludeWholeCalendar = 1
$exporter.IncludeAttachments = 1
$exporter.IncludePrivateDetails = 1
$exporter.RestrictToWorkingHours = 0
$exporter.SaveAsICal($output_file)