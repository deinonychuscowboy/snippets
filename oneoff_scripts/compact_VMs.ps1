param(
    $vm
)

if($vm -eq $null){
    $vm="*"
}

Get-Item $env:USERPROFILE\VirtualBox` VMs\$vm\*.vdi | ForEach { $_.FullName } | ForEach {  C:\Program` Files\Oracle\VirtualBox\VBoxManage.exe modifymedium disk $_ --compact }
Get-Item $env:USERPROFILE\VirtualBox` VMs\$vm\Snapshots\*.vdi | ForEach { $_.FullName } | ForEach {  C:\Program` Files\Oracle\VirtualBox\VBoxManage.exe modifymedium disk $_ --compact }