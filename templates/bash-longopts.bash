#!/usr/bin/env bash
# This template allows a bash executable to handle long-style "$ script --foobar baz" and "$ script --foobar=baz" options with just getopts (which normally supports only short-style "$ script -f baz" or "$script -fbaz" options)
# It converts long options to short options in the option processing step and then allows you to handle them like normal getopts short options on the second pass through

# OPTSPEC takes the form o=option for options that do not take arguments, and o:option for ones that do, space-separated
OPTSPEC="n=noarg a:arg"
# the subshell here converts our opspec into a getopts-compatible one
while getopts -- "-:$(echo $OPTSPEC | sed "s/:[^ ]*/:/g" | sed "s/=[^ ]*//g")" OPTCHAR; do
    OPTCHAROLD=""
    # after matching a long option the first time, we want to come back and process the short option it converts to the second time. Not using fall-through case terminators for compatibility
	while [ "$OPTCHAROLD" != "$OPTCHAR" ]; do
		OPTCHAROLD="$OPTCHAR"
        case "$OPTCHAR" in
        	# an optchar of dash means this is a long option that needs to be converted
            -)
                # go through the specification list that maps short to long options
				for OPT in $OPTSPEC; do
					OPTSHORT="$(echo $OPT | sed 's/[:=][^ ]*//g' )"
					OPTLONG="$(echo $OPT | sed 's/[^ ]*[:=]//g' )"
					OPTTYPE="$(echo $OPT | sed 's/[^ ]*\([:=]\)[^ ]*/\1/g' )"
					# when we find the long option (including if separated by an equal sign)
					if [ "$OPTLONG" == "$OPTARG" ] || [ "$OPTLONG=*" == "$OPTARG" ]; then
						# change optchar from - to the correct short option
						OPTCHAR="$OPTSHORT"
						# if this option takes an arg, change optarg to the correct arg
						if [ "$OPTTYPE" == ":" ]; then
							if [ "$OPTLONG" == "$OPTARG" ]; then
								# if the correct arg is separated with a space, increment and use the next potential option as the arg
								OPTARG="${!OPTIND}"; OPTIND=$(( $OPTIND + 1 ))
							else
								# if it's separated with an equal sign, just strip that bit off
								OPTARG="${OPTARG#*=}"
							fi
						else
						    if ! [ "$OPTLONG" == "$OPTARG" ]; then
								echo "$OPTARG\n$OPTLONG does not take an argument"
							fi
						fi
					fi
					counter=$(( $counter + 1 ))
				done
				# continue to process the short option we just converted from
				;;
			n)
				noarg="foo";
				;;
			a)
				arg="$OPTARG";
				;;
            # other shortform options go here
        esac
    done
done
